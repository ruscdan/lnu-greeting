import { useEffect, useRef, useState } from 'react'
import { useHistory } from 'react-router-dom'
import classes from './Home.module.css'
import Card from './UI/Card'
import CoolButton from './UI/CoolButton'
import state from '../state'

const Home = () => {
  const [studentName, setStudentName] = useState('')
  const history = useHistory()
  const input = useRef()

  const handleInputChange = event => setStudentName(event.target.value)

  const handleGreetingClick = (event) => {
    event.preventDefault()
    if (studentName.length !== 0) {
      state.currentTextMaterial = 'metallic'
      history.push('/scene', { studentName })
    }
  }

  useEffect(() => {
    input.current.focus()
  }, [])

  return (
    <div className={classes.home}>
      <Card>
        <p className={classes.title}>Please write your name</p>
        <p className={classes.subtitle}>(for a nice LNU greeting!)</p>
        <form onSubmit={handleGreetingClick}>
        <input 
          className={classes.nameInput} 
          type="text"
          value={studentName}
          onChange={handleInputChange}
          ref={input}
        />
        <CoolButton
          type="submit"
          buttonLabel='Get greeting!'
          centeredInRow
          // onClick={handleGreetingClick}
          isDisabled = {studentName.length === 0}
        />
        </form>
      </Card>
    </div>
  )
}

export default Home