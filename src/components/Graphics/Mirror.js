import { extend } from 'react-three-fiber'
import * as THREE from 'three'
import { Reflector } from 'three/examples/jsm/objects/Reflector'
extend({ Reflector })

const Mirror = (props) => {
  // const floor = new THREE.PlaneGeometry(10, 10)
  const floor = new THREE.SphereGeometry(3, 32, 32)
  const mirror = {
    clipBias: 0.003,
		textureWidth: window.innerWidth * window.devicePixelRatio,
		textureHeight: window.innerHeight * window.devicePixelRatio,
		color: 0x777777
  }

  return (
    <reflector {...props}
      args={[floor, mirror]}
    />
  )
}

export default Mirror