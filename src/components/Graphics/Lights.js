const Lights = () => {
  return(
    <>
      <ambientLight intensity={0.3} />
      <pointLight 
        position={[2, 7, -1.5]}
        castShadow
      />
      <pointLight 
        position={[-2, 3, 3]}
        castShadow
      />
    </>
  )
}

export default Lights