const Floor = (props) => {
  return (
    <mesh {...props} receiveShadow>
      <boxBufferGeometry args={[20, 0.5, 10]}/>
      <meshPhysicalMaterial/>
    </mesh>
  )
}

export default Floor