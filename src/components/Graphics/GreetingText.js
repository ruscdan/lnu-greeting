import { useRef } from "react"
import state from '../../state'

import { useLoader, useFrame } from "react-three-fiber"
import * as THREE from 'three'

const GreetingText = (props) => {
  const text = useRef()
  const font = useLoader(
    THREE.FontLoader, 
    process.env.PUBLIC_URL + '/fonts/Helvetiker.json'
  )
  const textOptions = {
    font,
    size: 1,
    height: 0.2,
    bevelEnabled: true,
    bevelThickness: 0.05,
    bevelSize: 0.01
  }

  const woodTexture = useLoader(
    THREE.TextureLoader,
    process.env.PUBLIC_URL + '/textures/wood.jpg'
  )
  const bloodTexture = useLoader(
    THREE.TextureLoader,
    process.env.PUBLIC_URL + '/textures/blood.jpg'
  )

  useFrame(({ clock }) => {
    text.current.rotation.x = 
    text.current.rotation.y = 
    // text.current.rotation.z =
      Math.sin(clock.getElapsedTime()) * 0.3
    text.current.geometry.center()
    if (state.isTextMaterialChanged) {
      for (const [key, value] of Object.entries(state.textMaterials[state.currentTextMaterial])) {
        text.current.material[key] = value
      }
      if (state.currentTextMaterial === 'wood') {
        text.current.material.map = woodTexture
      }
      if (state.currentTextMaterial === 'blood') {
        text.current.material.map = bloodTexture
      }
      state.isTextMaterialChanged = false
    }
  })

  return (
    <mesh {...props} ref={text} 
      castShadow
      receiveShadow
    >
      <textGeometry args={[props.text, textOptions]}/>
      <meshPhysicalMaterial
        { ...state.textMaterials[state.currentTextMaterial] }
      />
    </mesh>
  )
}

export default GreetingText