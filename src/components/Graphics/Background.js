import { useFrame, useLoader, useThree } from "react-three-fiber"
import { useMemo } from "react"
import * as THREE from 'three'

const Background = () => {
  const texture = useLoader(
    THREE.TextureLoader,
    process.env.PUBLIC_URL + '/textures/student-room.jpg'
  )

  const { gl, scene } = useThree()
  
  const cubicTexture = useMemo( () =>
    new THREE.WebGLCubeRenderTarget(
      texture.image.height
    ).fromEquirectangularTexture(gl, texture)
  ,[gl, texture])

  useFrame(() => scene.rotation.y -= 0.002)

  return (
    <primitive
      attach='background'
      object={cubicTexture}
    />
  )
}

export default Background