import { useRef } from 'react'
import { extend, useFrame, useThree } from 'react-three-fiber'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
extend({ OrbitControls })

const Orbit = () => {
  const { camera, gl } = useThree()
  const controls = useRef()
  useFrame (() => controls.current.update())

  return (
    <orbitControls ref={controls}
      args={[camera, gl.domElement]}
      autoRotate
      autoRotateSpeed={1}
      // enabled={false}
    />
  )
}

export default Orbit