import state from '../../state'
import CoolButton from "../UI/CoolButton"

const MaterialPicker = () => {
  const materials = ['Metallic', 'Glass', 'Wood', 'Blood']

  const handleMaterialClick = event => {
    const textClickedButton = event.target.textContent.toLowerCase()
    state.currentTextMaterial = textClickedButton
    state.isTextMaterialChanged = true
  }

  return (
    <div>
      {
        materials.map(material => 
          <CoolButton 
            buttonLabel={material}
            key={material}
            onClick={handleMaterialClick}
          />
        )
      }
    </div>
  )
}

export default MaterialPicker