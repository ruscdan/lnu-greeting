import classes from './Scene.module.css'

import Lights from './Graphics/Lights'
import GreetingText from './Graphics/GreetingText'
import Orbit from './Graphics/Orbit'
import Background from './Graphics/Background'
import Mirror from './Graphics/Mirror'
import Model from './Graphics/Model'
import MaterialPicker from './Graphics/MaterialPicker'
import CoolButton from './UI/CoolButton'

import { useEffect } from "react"
import { useHistory, useLocation } from "react-router-dom"

import { Canvas } from "react-three-fiber"
import { Suspense, useState } from 'react'

const Scene = () => {
  const location = useLocation()
  const { studentName } = location.state || ''
  const history = useHistory()

  useEffect(() => {
    if (!studentName) {
      history.push('/')
    }
  }, [history, studentName])
  
  const [isVaderVisible, setIsVaderVisible] = useState(false)

  const handleToggleVaderClick = () => {
    setIsVaderVisible(!isVaderVisible)
  }

  const handleBackClick = () => {
    history.push('/')
  }

  return (
    <div className={classes.sceneContainer}>
      <div className={classes.sceneUIContainer}>
        <MaterialPicker />
        <CoolButton 
          buttonLabel={isVaderVisible ? 'Hide you room mate' : 'Show you room mate'}
          onClick={handleToggleVaderClick}
        />
        <CoolButton 
          buttonLabel='Go back'
          onClick={handleBackClick}
        />
      </div>
      <Canvas 
        className={classes.canvas}
        camera={ { position: [2, 4, 10] } }
        shadowMap
      >
        <Suspense fallback={null}>
          <Background />
        </Suspense>
        <Lights />
        <Suspense fallback={null}>
          <GreetingText 
            position={[0, 4, 0]}
            text={`Hello ${studentName}`}
          />
        </Suspense>
        <Mirror 
          position={[0, 2, -7]}
        />
        <Suspense fallback={null}>
          <Model 
            path='/models/Darth-Vader/scene.gltf'
            scale={[10, 10, 10]}
            position={[-8, -15, 0]}
            rotation={[0, Math.PI / 3, 0]}
            visible={isVaderVisible}
          />
        </Suspense>
        <Orbit />
      </Canvas>
    </div>
  )
}

export default Scene