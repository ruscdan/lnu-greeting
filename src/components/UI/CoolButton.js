import classes from './CoolButton.module.css';

const CoolButton = (props) => {
  const btnClasses = props.centeredInRow ? 
    classes.buttonCenteredInRow :
    classes.buttonNormal
  return (
    <button 
      className={`${classes.coolButton} ${btnClasses}`} 
      disabled={props.isDisabled}
      onClick={props.onClick}
    >
      {props.buttonLabel}
    </button>
  )
}

export default CoolButton