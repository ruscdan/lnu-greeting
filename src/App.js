import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './components/Home';
import Scene from './components/Scene';

import './App.css'

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/' exact>
          <Home />
        </Route>
       <Route path='/scene'>
          <Scene />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
