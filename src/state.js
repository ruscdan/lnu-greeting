import * as THREE from 'three'

const state = {
  textMaterials: {
    metallic: {
      metalness: 1,
      roughness: 0.7,
      clearcoat: 1,
      transmission: 0,
      transparent: false, 
      color: new THREE.Color('blue'),
      map: null
    },
    glass: {
      transmission: 0.7,
      transparent: true,
      metalness: 0,
      roughness: 1,
      clearcoat: 0,
      color: new THREE.Color('blue'),
      map: null
    },
    wood: {
      transmission: 0,
      transparent: false,
      metalness: 0,
      roughness: 1,
      clearcoat: 0,
      color: new THREE.Color('white')
    },
    blood: {
      transmission: 0,
      transparent: false,
      metalness: 0,
      roughness: 1,
      clearcoat: 0,
      color: new THREE.Color('white')
    }
  },
  currentTextMaterial: 'metallic',
  isTextMaterialChanged: false
}

export default state